package ru.krivotulov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add(@NotNull AbstractCommand command);

    AbstractCommand getCommandByName(@NotNull String name);

    AbstractCommand getCommandByArgument(@NotNull String argument);

}
