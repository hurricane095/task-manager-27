package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.List;

/**
 * IUserOwnedService
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
