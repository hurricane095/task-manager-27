package ru.krivotulov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<AbstractCommand> getTerminalCommands();

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

}
