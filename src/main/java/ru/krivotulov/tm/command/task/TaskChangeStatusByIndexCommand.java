package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;

/**
 * TaskChangeStatusByIndexCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-change-status-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.readLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final String userId = getUserId();
        getTaskService().changeStatusByIndex(userId, index, status);
    }

}
